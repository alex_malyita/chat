const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const webpack = require('webpack');

const ENV_CONFIG = require('./.env.js');

const ENV = process.env.NODE_ENV || 'development';

const CONFIG = {
  entry: {
    app: './client/index.js',
    product: './product.js',
  },
  output: {
    path: path.resolve('./../public/chat'),
    filename: (ENV === 'development') ? '[name].js' : '[name].min.js',
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.less$/, loader: 'style-loader!css-loader!less-loader' },
      { test: /\.(png|jpg|gif)$/, loader: 'url-loader' },
    ],
  },
  plugins: [],
};

if (ENV === 'production') {
  CONFIG.plugins.push(
    new UglifyJSPlugin({
      beautify: false,
      comments: false,
      compress: {
        sequences: true,
        booleans: true,
        loops: true,
        unused: true,
        warnings: false,
        drop_console: true,
        unsafe: true,
      },
    })
  );
  CONFIG.plugins.push(
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$/,
      threshold: 10240,
      minRatio: 0.8,
    })
  );
  CONFIG.plugins.push(
    new webpack.optimize.OccurrenceOrderPlugin()
  );
}

CONFIG.plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(ENV),
      SUPERVISOR: JSON.stringify(ENV_CONFIG.SUPERVISOR),
      SOCKET_SERVER: JSON.stringify(ENV_CONFIG.SOCKET_SERVER),
    },
  })
);

module.exports = CONFIG;
