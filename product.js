window.superChat = (function($) {
  require('./client/localStorage');
  const platform = require('platform');
  let $divLoader;
  const ENV = process.env.NODE_ENV || 'development';
  const scriptUrl = process.env.SUPERVISOR + '/chat/app.' + (ENV === 'production' ? 'min.' : '') + 'js';
  const errorsUrl = process.env.SUPERVISOR + '/api/chat/stat';
  const socketIo = process.env.SOCKET_SERVER + '/socket.io/socket.io.js';
  let appLoaded = false;
  let timerLoading = null;
  let timerLoader = null;
  const styles = {
    main: {
      position: 'absolute',
      background: '#fff',
      margin: 'auto',
      width: '203px',
      height: '136px',
      border: '1px solid #ccc',
      '-moz-border-radius': '3px',
      '-webkit-border-radius': '3px',
      'border-radius': '3px',
      '-moz-box-shadow': '0px 4px 8px 0px rgba(0, 0, 0, 0.15)',
      '-webkit-box-shadow': '0px 4px 8px 0px rgba(0, 0, 0, 0.15)',
      'box-shadow': '0px 4px 8px 0px rgba(0, 0, 0, 0.15)',
      display: 'none',
      'z-index': '9999',
    },
    loading: {
      height: '60px',
      width: '60px',
      margin: '22px auto 0px',
      background: 'url(/images/loading.gif) no-repeat',
      'font-size': '14px',
      'font-weight': 'bold',
      'text-align': 'center',
      padding: '22px 0 0 2px',
    },
    text: {
      color: '#424242',
      font: '1.14286em "Arial", sans-serif',
      'text-align': 'center',
    },
  };

  function sendError(type) {
    $.post(errorsUrl, {
      browser: `${platform.name} ${platform.version}`,
      os: platform.os.toString(),
      type,
    });
  }

  function injectScript(src) {
    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.async = true;
      script.src = src;
      script.addEventListener('load', resolve);
      script.addEventListener('error', () => {
        sendError(`error_inject_js ${src}`);
        reject('Error loading script.');
      });
      script.addEventListener('abort', () => {
        sendError(`abort_inject_js ${src}`);
        reject('Script loading aborted.');
      });
      document.head.appendChild(script);
    });
  }

  function center($div) {
    $div.css('position', 'absolute');
    $div.css('top', Math.max(0, (($(window).height() - $div.outerHeight()) / 2) +
        $(window).scrollTop()));
    $div.css('left', Math.max(0, (($(window).width() - $div.outerWidth()) / 2) +
        $(window).scrollLeft()));
    return $div;
  }

  function render() {
    $divLoader = $('.chat-loading');
    if (!$divLoader.length) {
      $divLoader = $('<div />')
        .addClass('chat-loading')
        .attr('id', 'chat-loader')
        .css(styles.main)
        .append(
          $('<div />').addClass('chat-loading__bg').css(styles.loading).text('0%'),
          $('<div />').addClass('chat-loading__text').css(styles.text).text('please wait...')
        )
        .appendTo($('body'));
    }
  }

  function handlerLoadScript(status) {
    if (status !== 200) {
      appLoaded = false;
      return sendError('get_app.js');
    }
    appLoaded = true;
    dispatchOpenChat();
  }

  function triggerEvent(el, eventName){
    let event;
    if (document.createEvent) {
      event = document.createEvent('HTMLEvents');
      event.initEvent(eventName, true, true);
    } else if (document.createEventObject) { // IE < 9
      event = document.createEventObject();
      event.eventType = eventName;
    }
    event.eventName = eventName;
    if (el.dispatchEvent) {
      el.dispatchEvent(event);
    } else if (el.fireEvent && htmlEvents['on' + eventName]) {// IE < 9
      el.fireEvent('on' + event.eventType, event);// can trigger only real event (e.g. 'click')
    } else if (el[eventName]) {
      el[eventName]();
    } else if (el['on'+eventName]) {
      el['on'+eventName]();
    }
  }

  function dispatchOpenChat() {
    const chatDomEl = document.getElementById('chat-app-wrapper');
    if (!chatDomEl) {
      return sendError('error_in_app.js 1');
    }
    triggerEvent(chatDomEl, 'open-chat');
    setTimeout(() => {
      if (!localStorage.getItem('chat-app-success')) {
        return sendError('error_in_app.js 2');
      }
      localStorage.removeItem('chat-app-success');
    }, 1000);
  }

  function startLoad(e) {
    e && e.preventDefault();
    if (appLoaded && appLoaded === 'pending') return false;

    center($divLoader);
    $divLoader.show().attr('chat-opening', 'true');

    if (!appLoaded) {
      appLoaded = 'pending';
      injectScript(socketIo)
        .then(() => {
          injectScript(scriptUrl)
            .then(() => handlerLoadScript(200))
            .catch(() => handlerLoadScript(404));
        })
        .catch(() => sendError('error_socket.io.js'));
    } else {
      dispatchOpenChat();
    }

    clearTimeout(timerLoading);
    timerLoading = setTimeout(function(){stopLoad();}, 30000);

    loaderCounter();
  }

  function stopLoad() {
    $divLoader.hide().removeAttr('chat-opening');
    clearTimeout(timerLoader);
    clearTimeout(timerLoading);
  }

  function loaderCounter(i){
    i = i || 0;
    if(i > 100) return false;
    const $loaderCounterContainer = $divLoader.find('.chat-loading__bg');
    const n = Math.pow(1.082075, i);
    $loaderCounterContainer.html(i+'%');
    timerLoader = setTimeout(function(){loaderCounter(i + 1);}, n);
  }

  function checkChatState() {
    const state = localStorage.getItem('chat-app-state');
    if (state && (state === 'opened')) startLoad();
  }

  function init() {
    render();
    checkChatState();
  }

  init();

  return {
    startLoad,
    stopLoad,
  };
})($);
