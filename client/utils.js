import Storage from './services/storage';

const clearStorage = () => {
  Storage.remove(['state', 'uuid', 'tmp-user-name', 'assign-user']);
};

const nl2br = text => text.replace(/(?:\r\n|\r|\n)/g, '<br />');

const triggerEvent = (el, eventName) => {
  let event;
  if (document.createEvent) {
    event = document.createEvent('HTMLEvents');
    event.initEvent(eventName, true, true);
  } else if (document.createEventObject) { // IE < 9
    event = document.createEventObject();
    event.eventType = eventName;
  }
  event.eventName = eventName;
  if (el.dispatchEvent) {
    el.dispatchEvent(event);
  } else if (el.fireEvent && htmlEvents['on' + eventName]) {// IE < 9
    el.fireEvent('on' + event.eventType, event);// can trigger only real event (e.g. 'click')
  } else if (el[eventName]) {
    el[eventName]();
  } else if (el['on'+eventName]) {
    el['on'+eventName]();
  }
};

export {
  clearStorage,
  nl2br,
  triggerEvent,
};
