import React from 'react';
import PropTypes from 'prop-types';
import * as phrases from '../constants/TextPhrases';

const Preloader = props => (
  <div className="fetch-operator">
    <div className="wait-animation" />
    <div className="title">{phrases.QUEUE_TITLE}</div>
    { !props.queue ? (
      <span>{phrases.WAIT_TEXT}</span>
    ) : (
      <span dangerouslySetInnerHTML={{ __html: phrases.QUEUE_TEXT.replace('_QUEUE_', `<b>${props.queue}</b>`) }} />
    )}
  </div>
);

Preloader.propTypes = {
  queue: PropTypes.number.isRequired,
};

export default Preloader;
