import React from 'react';
import PropTypes from 'prop-types';
import * as linkify from 'linkifyjs';
import linkifyHtml from 'linkifyjs/html';
import { nl2br } from '../utils';

const Message = (props) => {
  const { msg, float } = props;
  return (
    <div className={`message ${float} ${msg.sendStatus === 'pending' ? 'loading' : ''}`}>
      <b>{msg.authorName}:</b>
      <div
        className="text"
        dangerouslySetInnerHTML={{ __html: linkifyHtml(nl2br(msg.message), { target: '_self' }) }}
      />
    </div>
  );
};

Message.propTypes = {
  msg: PropTypes.object,
  float: PropTypes.string,
};

Message.defaultProps = {
  msg: {
    authorName: PropTypes.string,
    message: PropTypes.string,
    authorType: PropTypes.string,
  },
};

export default Message;
