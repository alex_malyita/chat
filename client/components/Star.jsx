import React from 'react';
import PropTypes from 'prop-types';

const Star = (props) => {
  const nameMap = {
    isActive: 'is-active',
    isHover: 'is-hover',
  };
  const className = Object.keys(nameMap)
    .filter(prop => props[prop])
    .map(prop => nameMap[prop])
    .join(' ');
  const { onClick, onMouseEnter } = props;
  return <span className={className} onClick={onClick} onMouseEnter={onMouseEnter} />;
};

Star.defaultProps = {
  isActive: false,
};

Star.propTypes = {
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
};

export default Star;
