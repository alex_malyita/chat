import React from 'react';
import * as phrases from '../constants/TextPhrases';

class Rate extends React.Component {
  render() {
    const { onClose, onToggleConfirmClose } = this.props;
    return (
      <div className="rate-block preventing">
        <div className="title prevent">{phrases.CLOSE_PREVENTING_TITLE}</div>
        <p>{phrases.CLOSE_PREVENTING_DESC}</p>
        <div className="buttons">
          <div
            className="c-btn-grey btn-close-chat"
            onClick={() => onClose()}
          >
            {phrases.CLOSE}
          </div>
          <div
            className="c-btn-orange btn-keep-chatting"
            onClick={() => onToggleConfirmClose()}
          >
            {phrases.KEEP_CHATTING}
          </div>
        </div>
      </div>
    );
  }
}

export default Rate;
