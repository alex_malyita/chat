import React from 'react';
import PropTypes from 'prop-types';
const uuidV4 = require('uuid/v4');
import * as linkify from 'linkifyjs';
import linkifyHtml from 'linkifyjs/html';
import { nl2br } from '../utils';

const Attachment = (props) => {
  const { msg, float } = props;
  return (
    <div className={`message attachment ${float} ${msg.sendStatus === 'pending' ? 'loading' : ''}`}>
      <b>{msg.authorName}:</b>
      {msg.message &&
      <div
        className="text"
        dangerouslySetInnerHTML={{ __html: linkifyHtml(nl2br(msg.message), { target: '_self' }) }}
      />
      }
      <div className="attachment-list">
        {msg.attachments.map((attachment) => {
          return (
            <div key={uuidV4()} className="attachment-container">
              <a
                href={attachment.link}
                download="download"
                target="_blank"
              >
                {attachment.name}
              </a>
            </div>
          );
        })}
      </div>
    </div>
  );
};

Attachment.propTypes = {
  msg: PropTypes.object,
  float: PropTypes.string,
};

Attachment.defaultProps = {
  msg: {
    authorName: PropTypes.string,
    message: PropTypes.string,
    authorType: PropTypes.string,
    link: PropTypes.string,
    name: PropTypes.string,
  },
};

export default Attachment;
