import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Star from '../components/Star.jsx';
import { toggleConfirmClose } from '../actions';
import * as phrases from '../constants/TextPhrases';

class Rate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rate: 0,
      lastRate: 0,
      hover: 0,
      email: '',
      saveHistory: false,
    };
    this.onCancelRate = this.onCancelRate.bind(this);
    this.onStartRate = this.onStartRate.bind(this);
  }
  onStartRate() {
    const { rate } = this.state;
    this.setState({ lastRate: rate, rate: 0 });
  }
  onCancelRate() {
    const { lastRate } = this.state;
    this.setState({ rate: lastRate, hover: 0 });
  }
  setRate(rate) {
    this.setState({ lastRate: rate });
  }
  handlerMouseEnter(rate) {
    this.setState({ hover: rate });
  }
  render() {
    const { dispatch, onClose, showEmailInput } = this.props;
    const { rate, hover, saveHistory, email } = this.state;
    const nodes = Array.from(new Array(5), (x, i) => {
      const star = i + 1;
      const starProps = {
        key: `star-${star}`,
        isActive: star <= rate,
        isHover: star <= hover,
        onClick: this.setRate.bind(this, star),
        onMouseEnter: this.handlerMouseEnter.bind(this, star),
      };
      return <Star {...starProps} />;
    });
    return (
      <div className="rate-block">
        <form>
          <div className="title set_rate">{phrases.CLOSE_TITLE}</div>
          <p className="close-text">{phrases.CLOSE_TEXT}</p>
          <input
            name="save_history"
            id="save-history"
            type="checkbox"
            ref={el => this.checkbox = el}
            onClick={e => this.setState({ saveHistory: e.target.checked })}
          />
          <label htmlFor="save-history">{phrases.CLOSE_SAVE_TITLE}</label>
          { showEmailInput && saveHistory &&
            <input
              name="chat-user-email"
              type="email"
              ref={el => this.email = el}
              onChange={e => this.setState({ email: e.target.value.trim() })}
            />
          }
          <div className="set-rate">
            <p>{phrases.CLOSE_RATE_TITLE}</p>
            <div
              className="stars"
              onMouseEnter={this.onStartRate}
              onMouseLeave={this.onCancelRate}
            >
              {nodes}
            </div>
          </div>
          <div className="buttons">
            <div
              className="c-btn-grey btn-goback-chat"
              onClick={() => dispatch(toggleConfirmClose())}
            >
              {phrases.GO_BACK}
            </div>
            <input
              type="submit"
              className="c-btn-orange btn-close-chat"
              onClick={(e) => {
                e.preventDefault();
                onClose({ rate, email, save: +this.checkbox.checked });
              }}
              value={phrases.CLOSE_BTN_SUBMIT}
            />
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  showEmailInput: state.user.type === 'unregistered',
});

Rate.propTypes = {
  dispatch: PropTypes.func,
  showEmailInput: PropTypes.bool,
  onClose: PropTypes.func,
};

export default connect(mapStateToProps)(Rate);
