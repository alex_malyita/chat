import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Draggable from 'react-draggable';

import Form from './Form.jsx';
import MessageList from './MessageList.jsx';
import Preloader from '../components/Preloader.jsx';
import ConfirmClose from '../containers/ConfirmClose.jsx';
import Welcome from '../containers/Welcome.jsx';
import * as phrases from '../constants/TextPhrases';
import * as actions from '../actions';
import socket from '../socket';
import ee from '../ee';
import { clearStorage } from '../utils';
import Storage from '../services/storage';

const widthChat = 320;

let posChat = null;

class App extends React.Component {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
    const startLeftOffset = document.body.clientWidth - widthChat - 10;
    let findPos = [];
    if (Storage.get('pos')) {
      findPos = Storage.get('pos').split(',');
    }

    posChat = {
      x: parseInt(findPos[0]) || startLeftOffset,
      y: parseInt(findPos[1]) || 10,
    };

    this.handlerDragStop = this.handlerDragStop.bind(this);
    this.handlerWindowResize = this.handlerWindowResize.bind(this);

    window.addEventListener('resize', () => this.handlerWindowResize());

    let userType = 'unregistered';
    const userData = window.pdffiller;
    if (Storage.get('assign-user')) {
      // If we have chat reassign session for unregistered
      dispatch(actions.setUserId(Storage.get('assign-user')));
      userType = 'registered';
    } else if (userData && userData.user_id) {
      // If user is logged
      dispatch(actions.setUserId(userData.user_id));
      userType = 'registered';
    } else if (Storage.get('uuid')) {
      // If we have opened chat session for temp user
      dispatch(actions.loadChat());
    } else {
      // Show Welcome
      dispatch(actions.setChatLoading(false));
    }
    dispatch(actions.setUserType(userType));
    Storage.set('state', 'opened');
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch } = this.props;
    if (this.props.userId !== nextProps.userId) {
      dispatch(actions.loadChat());
    }
    if (this.props.chatRoom !== nextProps.chatRoom) {
      socket.private(nextProps.chatRoom)
        .listen('Chat.NewChatMessage', data => dispatch(actions.addMessage(data)))
        .listen('Chat.NewChatOpened', (data) => {
          dispatch(actions.setChatLoading(false));
          dispatch(actions.addMessage(data));
        })
        .listen('Chat.NewChatClosed', (data) => {
          dispatch(actions.closeChat(data));
          dispatch(actions.addMessage({
            author_name: data.author_name,
            author_type: 'Staff',
            message: phrases.CHAT_CLOSED,
          }));
          clearStorage();
        })
        .listen('Chat.AssignUserToChat', data => dispatch(actions.assignUser(data)))
        .listenForWhisper('typing', () => ee.emitEvent('typing'))
        .listenForWhisper('ping-connection', () => {
          socket.private(nextProps.chatRoom).whisper('client-ping-connection');
        });
    }
  }
  handlerDragStop(e, position) {
    Storage.set('pos', `${position.x},${position.y}`);
  }
  handlerWindowResize() {
    if (this.element) {
      const { left, top } = this.element.getBoundingClientRect();
      this.fixPosition(left, top);
    }
  }
  fixPosition(left, top) {
    const { clientWidth, clientHeight } = document.body;
    const { offsetWidth, offsetHeight } = this.element;
    const pos = { left, top };

    if (left < 0) {
      pos.left = 0;
    } else if (left > (clientWidth - offsetWidth)) {
      pos.left = clientWidth - offsetWidth;
    }
    if (top < 0) {
      pos.top = 0;
    } else if (top > (clientHeight - offsetHeight)) {
      pos.top = clientHeight - offsetHeight;
    }

    this.Drag.setState({
      x: pos.left,
      y: pos.top,
    });
  }
  render() {
    const { dispatch, queue, loading, showWelcome } = this.props;
    return (
      <Draggable
        ref={el => this.Drag = el}
        handle=".chat-header"
        bounds="body"
        defaultPosition={{ x: posChat.x, y: posChat.y }}
        onStop={this.handlerDragStop}
      >
        <div
          id="chat-app"
          ref={el => (this.element = el)}
        >
          <div className="chat-wrapper">
            <div className="chat-header">
              {phrases.TITLE}
              <div
                className="close-chat"
                onClick={() => dispatch(actions.toggleConfirmClose())}
              >x</div>
            </div>
            <div className="chat-body">
              <ConfirmClose />
              { !loading && showWelcome && <Welcome /> }
              { loading && !showWelcome && <Preloader queue={queue} /> }
              { !loading && !showWelcome && (
                <div className="chat-app-dialog">
                  <MessageList />
                  <Form />
                </div>
              )}
            </div>
          </div>
        </div>
      </Draggable>
    );
  }
}

App.propTypes = {
  dispatch: PropTypes.func,
  queue: PropTypes.number,
  chatRoom: PropTypes.string,
  loading: PropTypes.bool,
  userId: PropTypes.any,
  showWelcome: PropTypes.bool,
};

const mapStateToProps = state => ({
  queue: state.chat.queue,
  chatRoom: state.chat.room,
  loading: state.chat.loading,
  userId: state.user.id,
  showWelcome: !state.user.name,
});

export default connect(mapStateToProps)(App);
