import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';

import { sendMessage, setOperatorTyping, sendAttachment } from '../actions';
import socket from './../socket';
import ee from '../ee';
import * as phrases from '../constants/TextPhrases';
import { acceptedFIleTypes } from '../config';

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.typing = this.typing.bind(this);
    this.onDropAccepted = this.onDropAccepted.bind(this);
    this.takeControl = this.takeControl.bind(this);
    this.giveControl = this.giveControl.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);

    ee.addListener('typing', this.typing);
  }

  componentDidMount() {
    this.loadInterval = () => {
      clearTimeout(window.clearTyping);
      window.clearTyping = setTimeout(() => this.props.dispatch(setOperatorTyping(false)), 3000);
    };
  }
  onDropAccepted(files) {
    this.props.dispatch(sendAttachment(files));
  }
  typing() {
    this.props.dispatch(setOperatorTyping(true));
    this.loadInterval && this.loadInterval();
  }

  takeControl() {
    if (typeof (ActionCreators) != 'undefined' && typeof (Controllers) != 'undefined') {
      ActionCreators.Input.setCurrentController(Controllers.ShopappController);
      document.addEventListener('keyup', this.handleKeyUp, true, true);
    }
  }
  giveControl() {
    if (typeof (ActionCreators) != 'undefined'){
      ActionCreators.Input.cancelCurrentController();
      document.removeEventListener('keyup', this.handleKeyUp, true, true);
    }
  }

  handleKeyUp(event) {
    if (!event.shiftKey && event.key === 'Enter')
      this.handleSubmit(event);
  }

  handleSubmit(event) {
    event.preventDefault();
    const message = this.input.value.trim();
    if (!message.length) return false;
    this.input.value = '';
    return this.props.dispatch(sendMessage(message));
  }
  handleKeyPress(event) {
    const { chatId, chatRoom } = this.props;
    if (!chatId) return;
    if (!event.shiftKey && event.key === 'Enter') {
      this.handleSubmit(event);
      return;
    }
    socket.private(chatRoom)
      .whisper('typing', {
        name: this.props.user.name,
        chat_id: chatId,
        message: event.target.value,
      });
  }
  render() {
    return (
      <form className="chat-form" onSubmit={this.handleSubmit}>
        <div
          className="typing"
          style={{ display: this.props.chatTyping ? 'block' : 'none' }}
        >
          {phrases.OPERATOR_TYPING}
        </div>
        <Dropzone
          style={{ display: 'none' }}
          accept={acceptedFIleTypes}
          ref={(el) => { this.dropzoneRef = el; }}
          onDropAccepted={this.onDropAccepted}
        >
          <p>Drop files here.</p>
        </Dropzone>
        { !this.props.chatClosed &&
        <div className="enter-msg">
          <textarea
            onFocus={this.takeControl}
            onBlur={this.giveControl}
            onKeyPress={this.handleKeyPress}
            ref={(el) => { this.input = el; }}
            placeholder={phrases.TYPE_MESSAGE}
          />
          <div className="enter-btn c-btn-orange" onClick={this.handleSubmit}>Send</div>
          <div className="attach-file" onClick={() => this.dropzoneRef.open()} />
        </div>
        }
      </form>
    );
  }
}

Form.propTypes = {
  chatId: PropTypes.number,
  chatRoom: PropTypes.string,
  chatClosed: PropTypes.bool,
  chatTyping: PropTypes.bool,
  user: PropTypes.object,
};

const mapStateToProps = state => ({
  chatId: state.chat.id,
  chatClosed: state.chat.closed,
  chatRoom: state.chat.room,
  chatTyping: state.chat.typing,
  user: state.user,
});

export default connect(mapStateToProps)(Form);
