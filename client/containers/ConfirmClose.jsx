import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { clearStorage, triggerEvent } from '../utils';

import { sendCloseChat, toggleConfirmClose } from '../actions/index';
import Rate from './Rate.jsx';
import PreventedChatting from '../components/PreventedChatting.jsx';
import socket from '../socket';

class ConfirmClose extends React.Component {
  constructor(props) {
    super(props);
    this.handlerClose = this.handlerClose.bind(this);
  }
  handlerClose(data = {}) {
    triggerEvent(document.getElementById('chat-app-wrapper'), 'close-chat');
    this.props.dispatch(sendCloseChat(data));
    socket.private(this.props.chatRoom)
      .whisper('activity', {
        author_name: this.props.userName,
        chat_id: this.props.chatId,
        type: 'close_chat',
        rate: data.rate ? data.rate : false,
      });
    clearStorage();
  }
  render() {
    const { dispatch, showRate, show } = this.props;
    return (
      <div
        className="confirm-chat"
        style={{ display: show ? 'block' : 'none' }}
      >
        { showRate ? (
          <Rate onClose={this.handlerClose} />
        ) : (
          <PreventedChatting
            onClose={this.handlerClose}
            onToggleConfirmClose={() => dispatch(toggleConfirmClose())}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  show: state.chat.confirmClose,
  chatRoom: state.chat.room,
  chatId: state.chat.id,
  userName: state.user.name,
  showRate: state.messages.length > 1,
});

ConfirmClose.propTypes = {
  dispatch: PropTypes.func,
  show: PropTypes.bool,
  chatRoom: PropTypes.string,
  chatId: PropTypes.number,
  userName: PropTypes.string,
  showRate: PropTypes.bool,
};

export default connect(mapStateToProps)(ConfirmClose);
