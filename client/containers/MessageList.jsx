import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Message from '../components/Message.jsx';
import Attachment from '../components/Attachment.jsx';
import { sendStat } from '../actions';

const uuidV4 = require('uuid/v4');

class MessageList extends React.Component {
  componentDidUpdate(props) {
    if (this.props.messages.length !== props.messages.length) {
      setTimeout(() => {
        this.element.scrollTop = this.element.scrollHeight;
      }, 0);
    }
  }
  componentDidMount() {
    this.element.scrollTop = this.element.scrollHeight;
    this.props.dispatch(sendStat('open-message-list'));
  }
  render() {
    return (
      <div
        className="messages-list"
        ref={el => (this.element = el)}
      >
        {this.props.messages.map((msg) => {
          const styleFloat = msg.authorType !== 'Staff' ? 'right' : 'left';
          if (msg.messageType === 'attachment') {
            return <Attachment float={styleFloat} key={uuidV4()} msg={msg} />;
          }
          return <Message float={styleFloat} key={uuidV4()} msg={msg} />;
        })}
      </div>
    );
  }
}

MessageList.propTypes = {
  messages: PropTypes.array,
  dispatch: PropTypes.func,
};

const mapStateToProps = state => ({
  messages: state.messages,
});

export default connect(mapStateToProps)(MessageList);
