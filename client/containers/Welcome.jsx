import React from 'react';
import { connect } from 'react-redux';
import * as phrases from '../constants/TextPhrases';
import { setUserName, loadChat, setChatLoading } from '../actions';

class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleKeyPress(event) {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
      return;
    }
  }
  handleSubmit(event) {
    const { dispatch } = this.props;
    const msg = this.input.value.trim();
    if (!msg.length) return false;
    dispatch(setUserName(msg));
    dispatch(loadChat());
  }
  render() {
    return (
      <div className="chat-welcome">
        <div className="title">{phrases.WELCOME_TITLE}</div>
        <p>{phrases.WELCOME_TEXT}</p>
        <input
          type="text"
          onKeyPress={this.handleKeyPress}
          ref={el => (this.input = el)}
          placeholder={phrases.WELCOME_INPUT_NAME}
        />
        <div className="enter-btn c-btn-orange" onClick={this.handleSubmit}>
          {phrases.WELCOME_SUBMIT}
        </div>
      </div>
    );
  }
}

export default connect()(Welcome);
