import Echo from 'laravel-echo';
import { socketServer } from './config';

export default new Echo({
  broadcaster: 'socket.io',
  host: socketServer,
  encrypted: true,
  namespace: 'App.Events',
});
