class Storage {

  constructor() {
    this._storage = window.localStorage;
    this._prefix = 'chat-app';
  }

  set(key, value) {
    this._storage.setItem(`${this._prefix}-${key}`, value);
  }

  remove(key) {
    if (Array.isArray(key)) {
      key.forEach((el) => {
        this._storage.removeItem(`${this._prefix}-${el}`);
      });
    } else {
      this._storage.removeItem(`${this._prefix}-${key}`);
    }
  }

  get(key) {
    return this._storage.getItem(`${this._prefix}-${key}`) || null;
  }

}

export default new Storage();
