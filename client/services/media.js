import { supervisor } from '../config';

const audio = Symbol();
const type = Symbol();
const sources = Symbol();

class Media {

  constructor() {
    this[audio] = new Audio();
    this[audio].volume = 1;

    this[type] = 'message';

    this[sources] = {
      message: `${supervisor}/audio/client/new-message.mp3`,
    };
  }

  play(type = null) {
    if (!type) type = 'message';

    this[audio].src = this[sources][type];
    this[audio].play();
  }

}

export default new Media();
