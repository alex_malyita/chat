import request from 'superagent';
import serializer from 'superagent-serializer';
import Fingerprint2 from 'fingerprintjs2';

import { supervisor } from './../config';
import Storage from '../services/storage';

const platform = require('platform');

const uuid = Storage.get('uuid');

serializer(request, 'camel');

const fingerprint = new Promise((resolve) => {
  if (Storage.get('fp')) {
    resolve(Storage.get('fp'));
  }
  const options = {
    excludeUserAgent: true,
    excludeAvailableScreenResolution: true,
    excludeScreenResolution: true,
    excludeJsFonts: true,
    excludeFlashFonts: true,
    excludePlugins: true,
  };
  Fingerprint2(options).get((result) => {
    Storage.set('fp', result);
    resolve(result);
  });
});

const fetchChat = (userId = false, userName = false) => {
  return fingerprint.then((fp) => {
    const data = {
      user_id: userId,
      user_name: userName,
      browser: `${platform.name} ${platform.version}`,
      os: platform.os.toString(),
      uuid,
      fp,
    };
    return request
      .post(`${supervisor}/api/chat/new`, 'POST')
      .set('Accept', 'application/json')
      .send(data)
      .then(res => res)
      .catch((err) => {
        console.log(err);
         // ToDo error notification
      });
  });
};

const sendMessage = (data) => {
  return request
    .post(`${supervisor}/api/chat/new-message`, 'POST')
    .set('Accept', 'application/json')
    .send(data)
    .then(res => res)
    .catch((err) => {
      console.log(err);
      // ToDo error notification
    });
};

const sendAttachment = (chatId, hash, uuid, files) => {
  const req = request.post(`${supervisor}/api/chat/new-attachment`)
    .field('chat_id', chatId)
    .field('hash', hash)
    .field('uuid', uuid);

  files.forEach(file => req.attach(file.name, file));

  return req.end((err, res) => {
    if (err) return console.log(err);
    // ToDo error notification
    return res;
  });
};

const sendCloseChat = (userId, chatId, hash, data = {}) => {
  return request
    .post(`${supervisor}/api/chat/new/close`, 'POST')
    .set('Accept', 'application/json')
    .send({ ...data, user_id: userId, chat_id: chatId, hash })
    .then(res => res)
    .catch((err) => {
      console.log(err);
      // ToDo error notification
    });
};

const sendActivity = (data) => {
  return request
    .post(`${supervisor}/api/chat/new-activity`, 'POST')
    .set('Accept', 'application/json')
    .send(data)
    .then(res => res)
    .catch((err) => {
      console.log(err);
      // ToDo error notification
    });
};

const sendStat = data => (
  request
    .post(`${supervisor}/api/chat/new-stat`, 'POST')
    .set('Accept', 'application/json')
    .send(data)
    .then(res => res)
    .catch((err) => {
      console.log(err);
      // ToDo error notification
    })
);

export default {
  fetchChat,
  sendMessage,
  sendCloseChat,
  sendAttachment,
  sendActivity,
  sendStat,
};
