import * as types from '../constants/ActionTypes';
import Storage from '../services/storage';
import Media from '../services/media';

const uuidV4 = require('uuid/v4');

const setMessages = messages => ({
  type: types.SET_MESSAGES,
  messages,
});

const setChatRoom = room => ({
  type: types.SET_CHAT_ROOM,
  room,
});

const setQueue = queue => ({
  type: types.SET_QUEUE,
  queue,
});

const setOperatorTyping = typing => ({
  type: types.SET_OPERATOR_TYPING,
  typing,
});

const setChatId = id => ({
  type: types.SET_CHAT_ID,
  id,
});

const closeChat = data => ({
  type: types.CLOSE_CHAT,
});

const setUserName = name => ({
  type: types.SET_USER_NAME,
  name,
});

const setUserId = id => ({
  type: types.SET_USER_ID,
  id,
});
const setUserType = userType => ({
  type: types.SET_USER_TYPE,
  userType,
});

const toggleConfirmClose = () => ({
  type: types.TOGGLE_CONFIRM_CLOSE,
});

const addMessage = data => (dispatch, getState) => {
  window.clearTyping && clearTimeout(window.clearTyping);
  const store = getState();
  dispatch(setOperatorTyping(false));

  if (data.uuid) {
    const findMsg = store.messages.filter(msg => msg.uuid === data.uuid);
    if (findMsg.length) {
      return dispatch({
        type: types.UPDATE_MESSAGE_SEND_STATUS,
        attachments: data.attachments || [],
        sendStatus: 'sent',
        uuid: data.uuid,
      });
    }
  }

  dispatch({
    type: types.ADD_MESSAGE,
    authorName: data.author_name,
    authorType: data.author_type,
    message: data.message,
    attachments: data.attachments || [],
    messageType: data.type,
    uuid: data.uuid || null,
  });

  Media.play();
};

const setChatLoading = (setState = null) => (dispatch, getState) => {
  const store = getState();
  let loading = !store.chat.loading;
  if (setState !== null) loading = setState;
  dispatch({
    type: types.SET_LOADING,
    loading,
  });
};

const setHash = hash => ({
  type: types.SET_HASH,
  hash,
});

const sendMessage = message => (dispatch, getState, api) => {
  const store = getState();
  const uuid = uuidV4();

  dispatch({
    type: types.ADD_MESSAGE,
    authorName: store.user.name,
    authorType: 'User',
    message,
    uuid,
  });

  api.sendMessage({
    user_id: store.user.id,
    chat_id: store.chat.id,
    hash: store.chat.hash,
    user_type: store.user.type,
    message,
    uuid,
  });
};

const loadChat = () => (dispatch, getState, api) => {
  const store = getState();
  const { id, name } = store.user;
  dispatch(setChatLoading(true));

  api.fetchChat(id, name).then((res) => {
    dispatch(setChatRoom(res.room));
    dispatch(setChatId(res.chatId));
    dispatch(setUserName(res.userName));
    if (!window.pdffiller || !window.pdffiller.user_id || !window.pdffiller.user_email) {
      Storage.set('tmp-user-name', res.userName);
      Storage.set('uuid', res.uuid);
    }
    dispatch(setHash(res.hash));
    if (res.messages && res.messages.length) dispatch(setMessages(res.messages));
    if (res.queue) {
      dispatch(setQueue(res.queue));
    } else {
      dispatch(setChatLoading(false));
    }
    dispatch(sendNavigate());
  });
};

const sendCloseChat = data => (dispatch, getState, api) => {
  const store = getState();
  api.sendCloseChat(store.user.id, store.chat.id, store.chat.hash, data);
};

const sendAttachment = files => (dispatch, getState, api) => {
  const store = getState();
  const uuid = uuidV4();

  dispatch({
    type: types.ADD_MESSAGE,
    authorName: store.user.name,
    authorType: 'User',
    messageType: 'attachment',
    message: '',
    uuid,
  });

  api.sendAttachment(store.chat.id, store.chat.hash, uuid, files);
};

const sendNavigate = () => (dispatch, getState, api) => {
  const path = window.location.href.replace(window.location.origin, '');
  if (Storage.get('last-page') && Storage.get('last-page') === path) return;

  const store = getState();
  Storage.set('last-page', path);
  api.sendActivity({
    chat_id: store.chat.id,
    hash: store.chat.hash,
    activity: 'navigate',
    path,
  });
};

const assignUser = data => (dispatch) => {
  Storage.set('assign-user', data.user_id);
  dispatch({
    type: types.SET_USER_ID,
    id: data.user_id,
  });
};

const sendStat = (type, data = {}) => (dispatch, getState, api) => {
  const store = getState();
  api.sendStat({
    ...data,
    chat_id: store.chat.id,
    hash: store.chat.hash,
    type,
  });
};

export {
  sendMessage,
  toggleConfirmClose,
  setOperatorTyping,
  setUserName,
  setUserId,
  addMessage,
  closeChat,
  loadChat,
  setChatLoading,
  sendCloseChat,
  setUserType,
  sendAttachment,
  assignUser,
  sendStat,
};
