export const TITLE = 'PDFfiller friendly support';
export const OPERATOR_TYPING = 'Operator is typing ...';
export const TYPE_MESSAGE = 'Type message ...';
export const CLOSE_CHAT = 'Close chat ?';
export const CLOSE = 'Close';
export const CHAT_CLOSED = 'Closed the chat';
export const GO_BACK = 'Go back';
export const KEEP_CHATTING = 'Keep chatting';
export const QUEUE_TITLE = 'Wait a moment';
export const OPERATOR_CONNECTED = 'Welcome to PDFfiller Support. How may I help you today?';
export const WAIT_TEXT = 'Waiting for an available PDFfiller support specialist. ' +
  'This should only take a moment.';
export const QUEUE_TEXT = 'We are looking for available support agent: you are _QUEUE_ in line.';
export const SEND_OFFLINE_MESSAGE = 'You have been waiting for a long time. You can wait some more ' +
  'or send a message offline.';
export const CLOSE_TITLE = 'Goodbye :)';
export const CLOSE_TEXT = 'You can save the chat. We will send it to your email address.';
export const CLOSE_SAVE_TITLE = 'Save this chat ?';
export const CLOSE_RATE_TITLE = 'Please rate your customer service experience:';
export const CLOSE_BTN_SUBMIT = 'Goodbye';
export const CLOSE_PREVENTING_TITLE = 'Thank you for contacting PDFfiller!';
export const CLOSE_PREVENTING_DESC = 'Closing this chat window will result in the chat being over.';
export const WELCOME_TITLE = 'Welcome!';
export const WELCOME_TEXT = 'We’re here to answer all of your questions. Input your name below so we know what to call you.';
export const WELCOME_INPUT_NAME = 'Name';
export const WELCOME_SUBMIT = 'Lets Get Started';
