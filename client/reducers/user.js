import * as types from '../constants/ActionTypes';

const initialState = {
  name: '',
  id: null,
  type: 'unregistered',
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER_NAME:
      return { ...state, name: action.name };
    case types.SET_USER_ID:
      return { ...state, id: action.id };
    case types.SET_USER_TYPE:
      return { ...state, type: action.userType };
    default :
      return state;
  }
};

export default user;
