import * as types from '../constants/ActionTypes';
const uuidV4 = require('uuid/v4');

const message = (state, action) => {
  switch (action.type) {
    case types.ADD_MESSAGE:
      return {
        authorName: action.authorName,
        authorType: action.authorType,
        message: action.message,
        attachments: action.attachments || [],
        messageType: action.messageType || 'message',
        uuid: action.uuid || uuidV4(),
        sendStatus: action.authorType !== 'Staff' ? 'pending' : 'sent',
      };
    default :
      return state;
  }
};

const messages = (state = [], action) => {
  switch (action.type) {
    case types.ADD_MESSAGE:
      return [
        ...state,
        message(undefined, action),
      ];
    case types.SET_MESSAGES:
      return action.messages.map(data => ({
        authorName: data.authorName,
        authorType: data.authorType,
        message: data.message,
        attachments: data.attachments || [],
        messageType: data.type || 'message',
        uuid: data.uuid,
        sendStatus: 'sent',
      }));
    case types.UPDATE_MESSAGE_SEND_STATUS:
      return state.map(msg => {
        if (msg.uuid === action.uuid) {
          return {
            ...msg,
            sendStatus: action.sendStatus,
            attachments: action.attachments || msg.attachments,
          };
        }
        return msg;
      });
    default :
      return state;
  }
};

export default messages;
