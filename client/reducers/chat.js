import * as types from '../constants/ActionTypes';

const initialState = {
  id: 0,
  room: '',
  hash: '',
  queue: 0,
  typing: false,
  closed: false,
  loading: true,
  confirmClose: false,
};

const chat = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_CHAT_ROOM:
      return { ...state, room: action.room };
    case types.SET_CHAT_ID:
      return { ...state, id: action.id };
    case types.SET_QUEUE:
      return { ...state, queue: action.queue };
    case types.TOGGLE_CONFIRM_CLOSE:
      return { ...state, confirmClose: !state.confirmClose };
    case types.SET_LOADING:
      return { ...state, loading: action.loading };
    case types.CLOSE_CHAT:
      return { ...state, closed: true };
    case types.SET_HASH:
      return { ...state, hash: action.hash };
    case types.SET_OPERATOR_TYPING:
      return { ...state, typing: action.typing };
    default :
      return state;
  }
};

export default chat;
