import { combineReducers } from 'redux';
import messages from './messages';
import chat from './chat';
import user from './user';

const rootReducer = combineReducers({
  messages,
  chat,
  user,
});

export default rootReducer;
