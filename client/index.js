require('es6-promise').polyfill();
require('./localStorage');

import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root.jsx';
import configureStore from './store/configureStore';
import './styles/app.less';
import Storage from './services/storage';

let container;

const unmountChat = () => {
  if (!container) return;
  ReactDOM.unmountComponentAtNode(container);
};

const renderChat = () => {
  const store = configureStore();

  ReactDOM.render(
    <Root store={store} />,
    container,
  );
  container.addEventListener('close-chat', unmountChat);
};

const handlerOpenChat = (event) => {
  event.preventDefault();
  renderChat();
  superChat.stopLoad();
  Storage.set('success', 1);
};

const init = () => {
  container = document.getElementById('chat-app-wrapper');
  if (!container) {
    container = document.createElement('div');
    container.id = 'chat-app-wrapper';
    document.body.appendChild(container);
  }
  container.addEventListener('open-chat', handlerOpenChat);
};

init();
